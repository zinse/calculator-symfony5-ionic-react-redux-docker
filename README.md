# Symfony 5 REST API skeleton + docker + Ionic + react

Calculator: Docker + Backend (Symfony 5 + FOSRestBundle + JSON Standard responses) + FrontEnd (Ionic 6 + React + Redux) 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine 
for development and testing purposes. 

### Prerequisites

What things you need to install the software and how to install them.
- PHP 7.2.5+
- [composer](https://getcomposer.org/download/)
- [symfony](https://symfony.com/doc/current/setup.html)
- docker (optional)

### Installing

```bash
git clone https://gitlab.com/zinse/calculator-symfony5-ionic-react-redux-docker
cd calculator-symfony5-ionic-react-redux-docker
composer install
symfony server:start
## Front end dir
cd ui 
## Install Ionic Framework globally
npm npm install --unsafe-perm -g @ionic/cli native-run cordova-res
npm install
ionic serve
## Should open a windows at http://127.0.0.1:8100
```
### Installing (alternative with Docker)

```bash
git clone https://gitlab.com/zinse/calculator-symfony5-ionic-react-redux-docker
cd calculator-symfony5-ionic-react-redux-docker
docker-compose up --build -d
```

#### Test backend with Curl

```bash
curl -X POST http://127.0.0.1/v1/calculator/calculate -H 'Content-Type : application/json' -v -d '{"data":"3 + 5(3^2 - 20)"}'
```

#### Test from FrontEnd

Open your Browser at http://127.0.0.1:8100
```

