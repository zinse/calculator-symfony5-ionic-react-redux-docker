<?php
/**
 *
 * This file is part of a repository on GitHub.
 *
 * (c) Riccardo De Martis <riccardo@demartis.it>
 *
 * <https://github.com/demartis>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

namespace App\Utils;

use App\Utils\Jttp\Exception\InternalJttpException;
use App\Utils\Jttp\Exception\MalformedJttpException;

class Calculator
{
    /** oparator list by priority */
    public const OPERATORS = [['^' => 2],['*' => 1,'/' => 1], ['+' => 1,'-' => 1]];

    /**
     * Calculator execute expression
     * @param string $input calculation expression
     * @return float
     * @throws \Exception
     */
    function execute(string $input) : float
    {
        if(is_array($operands = $this->parseCalculationString( $input ))){
            return $this->evaluate($operands);
        }

        throw new \Exception("Error Processing Request", 1);
    }

    /**
     * Calculator expression parser
     * @param string $input calculation expression
     * @return array
     */
    private function parseCalculationString(string $input) : array
    {
        $input = preg_replace('/\s+/', '', $input);//remove white spaces
        $calculation = [];
        $current = '';
        $stringArray = str_split($input);// Split expression to array
        $stringArrayLn = count($stringArray);
        $operators = [];
        foreach(self::OPERATORS as $op){
            $operators = array_merge($operators, array_keys($op));
        }
        $regex = '#\((([^()]+|(?R))*)\)#';// regex to extract contain inside parenthesis
        $brackets = 0;
        //Loop each character from expression 
        for ($i = 0; $i < $stringArrayLn; $i++) 
        {
            $ch = $stringArray[$i];
            //extract data from parenthesis recursively
            if($ch == '(' && $brackets < 1)
            {
                if ($current != '') 
                {
                    $calculation[] = $current;
                    if(!in_array($current, $operators)){
                        $calculation[] = '*';
                    }
                    
                    $current = '';
                }

                $brackets += 1;

                if (preg_match($regex, substr($input, $i) ,$matches)) 
                {
                    $endBracketIndex = $i + strlen($matches[0]) - 1;
                    $calculation[] = $this->parseCalculationString($matches[1]);
                }
            }
            else if($ch == ')' && $brackets > 1) 
            {
                $brackets -= 1;
            }
        
            if(isset($endBracketIndex) && $i == $endBracketIndex)
            {
                $brackets = 0;
            }

            if($brackets > 0 || $ch == '(' || $ch == ')')
            {
                continue;
            }
        
            if (in_array($ch, $operators)) 
            {
                if ($current == '' && $ch == '-') 
                {
                    $current = '-';
                } 
                else 
                {
                    if ($current != '') 
                    {
                        $calculation[] = $current;
                    }

                    $calculation[] = $ch;
                    $current = '';
                }
            } 
            else {
                $current .= $ch;
            }
            
        }

        if ($current != '') 
        {
            $calculation[] = $current;
        }
    
        return $calculation;
    }

    /**
     * Evaluate array representation of the user calculation input
     * @param array operands array of item in calculation input
     * @return array 
     * @throws \Exception
     * 
     */
    private function evaluate(array $operands ) : float
    {
        $operators = self::OPERATORS;
        $subOperands = [];
        $operatorLn = count($operators);
        $operandLn = count($operands);
        
        for ($i = 0; $i < $operatorLn; $i++) 
        {//loop through operators by depth|priority and calculate
            for ($j = 0; $j < $operandLn; $j++) {
                if(is_array($operands[$j]))
                {
                    $operands[$j] = $this->evaluate($operands[$j]);
                }
                
                if(strpos($operands[$j],'%') !== FALSE)
                {
                    $operands[$j] = floatVal($operands[$j]) / 100;
                }
                
                if (isset($operators[$i][$operands[$j]])) 
                {
                    $currentOp = $operands[$j];
                } 
                else if (!empty($currentOp)) 
                {
                    $subOperands[count($subOperands) - 1] = $this->calculate(floatVal($subOperands[count($subOperands) - 1]), $currentOp, floatVal($operands[$j])); 
                    $currentOp = null;
                } 
                else 
                {
                    $subOperands[] = $operands[$j];
                }
            }
            
            $operands = $subOperands;
            $operandLn = count($operands);
            $subOperands = [];
        }
        
        if (count($operands) > 1) {
            throw new \Exception('Error: unable to resolve calculation');
        } 
        else 
        {
            return $operands[0];
        }
    }

    /**
     * Calculator execute operations 
     * @param float $a first operand
     * @param float $op operator (^*+-/)
     * @param float $b second operand 
     * @return float
     */
    private function calculate(float $a, string $op, float $b) : float
    {
        switch($op){
            case '^': 
                return pow($a, $b);
            case '*': 
                return $a * $b;
            case '/': 
                return $a / $b;
            case '+': 
                return $a + $b;
            case '-': 
                return $a - $b;
        }
    }
}