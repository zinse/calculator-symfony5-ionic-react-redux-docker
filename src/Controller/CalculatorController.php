<?php

namespace App\Controller;

use App\Exception\FormException;
use App\Utils\Calculator;
use FOS\RestBundle\Controller\AbstractFOSRestController;

use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;

use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/calculator")
 */
class CalculatorController extends AbstractFOSRestController
{

    /**
     * 
     * 
     * @Rest\Route("/calculate", name="calculator_post", methods={"POST"})
     * 
     * @Rest\View()
     * @param Request $request
     * @return View
     * @throws BadRequestHttpException
     */
    public function calculate(Request $request, Calculator $calculator){
      
        $body=json_decode($request->getContent(), true);
        
        if ($str = $body['data']){
            $result = $calculator->execute($str);
            $view = $this->view(['result' => $result], Response::HTTP_OK, []);
            
            return $this->handleView($view);

        } else {
            throw new BadRequestHttpException('Error Processing Request');
        }
    }
}
