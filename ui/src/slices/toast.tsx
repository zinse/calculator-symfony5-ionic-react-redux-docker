import { createSlice, nanoid, PayloadAction } from "@reduxjs/toolkit";

interface Item {
  id: string;
  color?: string;
  message: string;
  isVisible?: boolean;
}

const defaultOptions = {
  id: 0,
  color: "primary",
  message: "",
  isVisible: true,
  position:'bottom',
  duration: 2000,
};

//color: "primary", "success", "warning", "danger", "light","dark"
function createToast(options: Item) : Item
{
  return {
    ...defaultOptions,
    ...options
  }
}


export const toast = createSlice({
  name: 'toast',
  initialState: [] as Item[],
  reducers: {
    add: {
      reducer: (state, action: PayloadAction<Item>) => {
        state.push(action.payload)
      },
      prepare: (toast: Item) => {
        const id = nanoid(8)
        return { payload: { ...createToast(toast), id } }
      }
    },
    remove: {
      reducer: (state, action: PayloadAction<string>) => {
        state = state.filter(t => t.id !== action.payload)
      },
      prepare: (id: string) => {
        return { payload: id }
      }
    },
    clear: {
      reducer: (state, action: PayloadAction<any>) => {
        state = []
      },
      prepare: () => {
        return { payload: {} }
      }
    }
  }
})