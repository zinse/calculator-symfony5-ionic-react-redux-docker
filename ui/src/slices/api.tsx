import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import { API } from "../constants";

interface Ajax {
    url: string;
    method?: string;
    data?: any;
    accessToken?: string;
    onSuccess?: any;
    onFailure?: any;
    label?: string;
    headersOverride?: any;
}

const defaultParam = {
    url: "",
    method: "GET",
    data: {},
    accessToken: null,
    onSuccess: () => {},
    onFailure: () => {},
    label: "",
    headersOverride: null
};

export const ajax = (data: Ajax) => {

    return {
        type: API,
        payload: { ...defaultParam, ...data }
    };
}


export const api = createSlice({
    name: 'api',
    initialState: {
        isLoading: false,
        error: null,
        message: '',
        path: ''
    },
    reducers: {
      start: {
        reducer: (state, action: PayloadAction<string>) => {
            state.isLoading = true
            state.message = action.payload
        },
        prepare: (label: string) => {
          return { payload: label }
        }
      },
      end: {
        reducer: (state, action: PayloadAction<string>) => {
            state.isLoading = false
        },
        prepare: (label: string) => {
          return { payload: label }
        }
      },
      error: {
        reducer: (state, action: PayloadAction<any>) => {
            state.error = action.payload
        },
        prepare: (error: any) => {
          return { payload: error }
        }
      },
      accessDenied: {
        reducer: (state, action: PayloadAction<string>) => {
            state.path = action.payload
        },
        prepare: (url: string) => {
          return { payload: url }
        }
      }
    }
  })