import { combineReducers } from "@reduxjs/toolkit";
import { api } from './api';
import { toast } from './toast';
import { calculator } from './calculator';

const rootReducer = combineReducers({
    api: api.reducer,
    toast: toast.reducer,
    calculator: calculator.reducer
});

export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;