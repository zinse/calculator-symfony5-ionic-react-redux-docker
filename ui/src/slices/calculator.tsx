import {createSlice, PayloadAction} from '@reduxjs/toolkit'

export const calculator = createSlice({
    name: 'calculator',
    initialState: {
        input: '',
        result: '',
        prev: '',
    },
    reducers: {
      request: {
        reducer: (state, action: PayloadAction<string>) => {
            state.input = action.payload 
            state.result = ''
        },
        prepare: (label: string) => {
          return { payload: label }
        }
      },
      success: {
        reducer: (state, action: PayloadAction<number>) => {
          state.prev = state.input
          state.input = ''
          state.result = action.payload.toString()
        },
        prepare: (result: number) => {
          return { payload: result }
        }
      },
      clear: {
        reducer: (state, action: PayloadAction<any>) => {
          state.input = ''
          state.result = ''
          state.prev = ''
        },
        prepare: () => {
          return { payload: {} }
        }
      },
      error: {
        reducer: (state, action: PayloadAction<any>) => {
            state.prev = action.payload.error && action.payload.error.detail && action.payload.error.detail || 'ERROR'
        },
        prepare: (error: any) => {
          return { payload: error }
        }
      }
    }
  })