export const API_URI = 'http://127.0.0.1:8000/v1';
export const API = "API";
export const ACCESS_DENIED_MESSAGE = "You are not allowed to access this resource!";
export const GLOBAL_SUCCESS_MESSAGE = "Your request has been successfully processed!";
export const GLOBAL_ERROR_MESSAGE = "Oops! look like something went wrong";