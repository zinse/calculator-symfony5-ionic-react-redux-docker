import { Redirect, Route } from 'react-router-dom';
import {
  IonApp,
  IonContent,
  IonIcon,
  IonLabel,
  IonLoading,
  IonPage,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
  setupIonicReact
} from '@ionic/react';
import React, { useState, useEffect, PropsWithChildren, ComponentProps } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { IonReactRouter } from '@ionic/react-router';
import { home, ellipse, square, triangle, calculator } from 'ionicons/icons';
import { Toasts, Position, ToastProp,ToastsProps } from './components/Toasts';
import { toast } from './slices/toast';
import Home from './pages/Home';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';


setupIonicReact();

type Props = {
  history?: any
}; 

const App: React.FC<PropsWithChildren<Props>> = (props) => {

  const dispatch = useDispatch();
    
  useEffect(() => {
      dispatch(toast.actions.clear)
  }, []);

  const toasts = useSelector((state: any) => state.toast);
  const loader = useSelector((state: any) => state.api);
  
  return <IonApp>
    
    <IonReactRouter>
    <IonPage id="main">
      <IonContent>
      {
      <IonLoading
          isOpen={loader.isLoading}
          message={'Please wait...'}
          duration={3000}
          animated={true}
          spinner="lines" /* "bubbles" | "circles" | "circular" | "crescent" | "dots" | "lines" | "lines-small" | null | undefined */
      />
      }
      <Toasts toasts={toasts}  />
      <IonTabs>
        <IonRouterOutlet>
          <Route exact path="/home">
            <Home />
          </Route>
          <Route exact path="/">
            <Redirect to="/home" />
          </Route>
        </IonRouterOutlet>
        <IonTabBar slot="bottom">
          <IonTabButton tab="home" href="/home">
            <IonIcon icon={calculator} />
            <IonLabel>Calculator</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
      </IonContent>
      </IonPage>
    </IonReactRouter>
  </IonApp>
  
}

export default App;
