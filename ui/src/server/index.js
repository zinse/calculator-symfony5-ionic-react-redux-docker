import axios from "axios";
import { API, API_URI, GLOBAL_SUCCESS_MESSAGE, GLOBAL_ERROR_MESSAGE } from "../constants";
import { apiAccessDenied, apiError, apiStart, apiEnd } from "../actions/api";
import { addToast } from "../actions";

export const apiServer = (action, dispatch) =>
{

  if (action.type !== API) return;

  const {
    url,
    method,
    data,
    accessToken,
    onSuccess,
    onFailure,
    label,
    headers
  } = action.payload;
  const dataOrParams = ["GET", "DELETE"].includes(method) ? "params" : "data";

  // axios default configs
  //axios.defaults.baseURL = process.env.REACT_APP_BASE_URL || API_URI || "";
  axios.defaults.baseURL = url.startsWith(API_URI) ? "" : API_URI;
  axios.defaults.headers.common["Content-Type"] = "application/json";
  axios.defaults.headers.common["Authorization"] = `Bearer ${accessToken}`;

  if (label) {
    dispatch(apiStart(label));
  }

  return axios.request({
      url,
      method,
      headers,
      [dataOrParams]: data
    })
    .then(({ data }) => {

      if( typeof onSuccess === "function") {
          return onSuccess(data.data);
      }else{
        return data.data;
      }

  })
  .catch(error => {
    
    dispatch(apiError(error));

    //dispatch(addToast({message: GLOBAL_ERROR_MESSAGE, color: 'danger', position: 'bottom'}))

    if (error.response && error.response.status === 403) {
      dispatch(apiAccessDenied(window.location.pathname));
    }

    if(typeof onFailure === "function") {
      return onFailure(error);
    }

    return Promise.reject(error);

  })
  .finally(() => {
    if (label) {
      //dispatch(apiEnd(label));
    }
  });
    //.then(handleResponse);



  function handleResponse(response) {

      if (!response.ok) {

      const error = {message: (data && data.message) || response.statusText, errors: (data && data.errors) };
        dispatch(apiError(error));
        if ([401, 403].includes(response.status) /* && localStorage.getItem(USER_KEY)*/) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            //history.push('/login');
            dispatch(apiAccessDenied(window.location.pathname));
        }
          
        if(typeof onFailure === "function") {
          dispatch(onFailure(error));
        }
        
        if (label) {
          dispatch(apiEnd(label));
        }

        return Promise.reject(error);
      }
      
      if (label) {
        dispatch(apiEnd(label));
      }

      if(typeof onSuccess === "function") {
        return onSuccess(data);
      }

      return data;
  }
  /* return axios.request({
      url,
      method,
      headers,
      [dataOrParams]: data
    })
    .then(({ data }) => {
        console.log("je oasse ici")
        if( typeof onSuccess === "function") {
            
            return onSuccess(data.data);

        }else{
            console.log('je passe au promise')
            console.log(data.data);
            return data;
        }
        
      //dispatch(api(data));
    })
    .catch(error => {console.log(error)
      dispatch(apiError(error));
      if(typeof onFailure === "function") {
        dispatch(onFailure(error));
      }
      if (error.response && error.response.status === 403) {
        dispatch(apiAccessDenied(window.location.pathname));
      }
    })
    .finally(() => {
      if (label) {
        dispatch(apiEnd(label));
      }
    }); */
};