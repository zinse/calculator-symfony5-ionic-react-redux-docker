import { configureStore } from "@reduxjs/toolkit";
import rootReducer from "../slices";
import {useDispatch} from 'react-redux';
import apiMiddleware from "../middleware/api";

const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware: any) =>
  getDefaultMiddleware(
    {
      serializableCheck: {
        // Ignore these action types
        ignoredActions: ['API'],
        // Ignore these field paths in all actions
        //ignoredActionPaths: ['meta.arg', 'payload.timestamp'],
        // Ignore these paths in the state
        //ignoredPaths: ['items.dates'],
      },
    }
  ).concat(apiMiddleware),
  /*,
  preloadedState,
  enhancers: [monitorReducersEnhancer],*/
});

if (process.env.NODE_ENV === "development" && module.hot) {
  module.hot.accept("../slices", () => {
    const newRootReducer = require("../slices").default;
    store.replaceReducer(newRootReducer);
  });
}

export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>() // Export a hook that can be reused to resolve types
export default store;