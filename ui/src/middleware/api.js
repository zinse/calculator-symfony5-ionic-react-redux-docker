import axios from "axios";
import { API, API_URI, GLOBAL_ERROR_MESSAGE, GLOBAL_SUCCESS_MESSAGE } from "../constants";
import { api } from "../slices/api";
import { toast } from "../slices/toast";

const apiMiddleware = ({ dispatch }) => next => action => {
    
  next(action);

  if (action.type !== API) return;
  
  const {
    url,
    method,
    data,
    accessToken,
    onSuccess,
    onFailure,
    label,
    headers
  } = action.payload;
  const dataOrParams = ["GET", "DELETE"].includes(method) ? "params" : "data";

  // axios default configs
  //axios.defaults.baseURL = process.env.REACT_APP_BASE_URL || API_URI || "";
  axios.defaults.baseURL = url.startsWith(API_URI) ? "" : API_URI;
  axios.defaults.headers.common["Content-Type"] = "application/json";
  axios.defaults.headers.common["Authorization"] = `Bearer ${accessToken}`;

  //if (label) {
  dispatch(api.actions.start(label));
  //}

  axios
    .request({
      url,
      method,
      headers,
      [dataOrParams]: data
    })
    .then(({ data }) => {
        dispatch(toast.actions.add({id: 0,message: GLOBAL_SUCCESS_MESSAGE, color: 'success', position: 'bottom'}))
        dispatch(onSuccess(data.result));

        return data.result
    })
    .catch(error => {

        dispatch(toast.actions.add({id: 0,message: GLOBAL_ERROR_MESSAGE, color: 'danger', position: 'bottom'}))
        if (error.response) {
            error = error.response.data
        } else if (error.request) {
            error = {message: 'NETWORK ERROR'}
        }
        dispatch(onFailure(error))
        dispatch(api.actions.error(error));

        return error
    })
    .finally(() => {
      //if (label) {
        dispatch(api.actions.end(label));
      //}
    });
};

export default apiMiddleware;