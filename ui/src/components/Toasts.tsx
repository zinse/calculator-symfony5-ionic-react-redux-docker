import React, {  ComponentProps, PropsWithChildren } from "react";
import {Toast} from "./Toast";
import { IonBackdrop } from "@ionic/react";

export type Position = "bottom" | "middle" | "top";
export type ToastProp = {
    id: string,
    message: string,
    color?: "primary" | "success" | "warning" | "danger" | "light" | "dark",
    position?: Position,
    autoClose?: boolean
};

export type ToastsProps = {
    toasts: ToastProp[]
}

export const Toasts: React.FC<ToastsProps> = (props) => {
    
  const { toasts } = props;
  
  return (
    <>
        {/* {toasts.length && <IonBackdrop  />} */}
        {
            
            (toasts || []).map(toast => {
                const { id } = toast;
                return (
                    <Toast {...toast} key={id} /* onDismissClick={() => handleDismiss(id)} */ />
                );
            })
        }
    </>
    );
};

/*Toasts.propTypes = {
  actions: PropTypes.shape({
    removeToast: PropTypes.func.isRequired
  }).isRequired,
  toasts: PropTypes.arrayOf(PropTypes.object).isRequired
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ removeToast }, dispatch)
});

const mapStateToProps = state => ({
  toasts: state.toasts
});

export default connect(mapStateToProps, mapDispatchToProps)(Toasts);*/