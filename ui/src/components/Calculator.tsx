import {
    IonContent,
    IonButton,
    IonGrid,
    IonRow,
    IonCol
} from "@ionic/react";
import React, { useState, useEffect, PropsWithChildren } from "react";

import { useAppDispatch } from '../store'
import { useSelector } from "react-redux";
import { calculator } from "../slices/calculator";
import { ajax } from "../slices/api";


const Calculator: React.FC = () => {

    const dispatch = useAppDispatch();
    const operators = ['^', '÷', 'x', '+', '-'];
    const state = useSelector((state: any) => state.calculator)
    const [input, setInput] = useState('');
    useEffect(() => {
        setInput(state.input)
    },[state.result])
    
    const [op, setOp] = useState("");

    /**
     * Handle calculator button click
     * @param key string
     * 
     * @return any 
     */
    const onChangeHandler = (key: string/*, value: any*/) => {

        if (key == 'CE') {//clear last entry
            if (input.length) {
                if (op) {
                    setOp('');
                }
                setInput(input.slice(0, -1));
            }
        }
        else if (key == 'AC') {//clear all entry
            dispatch(calculator.actions.clear());
        }
        else if (operators.indexOf(key) > -1) {
            
            if (input.slice(-1) == '(' && key != '-' || input.slice(-1) == '.') {
                return false;
            }

            if (op) {//already register an operator
                if (op != '+' && op != '-' && key == '-') {
                    setInput(input + ' ' + key);
                } else {
                    setOp(key);
                    setInput(input.slice(0, -1) + key);
                }
            } else {
                if (!input) { //if input is empty and operator is -
                    if (key === '-') {
                        setInput((state.result && state.result + ' ') +key);
                        if(state.result){
                            setOp(key);
                        }
                    }else if( state.result){
                        setOp(key);
                        setInput((state.result + (key !== '^' ? ' ' : '')) +key);
                    }
                } else {
                    setOp(key);
                    setInput(input + ' ' + key);
                }
            }
        } else {
            if (input.slice(-1) == '%' && key === '.') {
                setInput(input + ' x ' + key);
            }
            else if (input.slice(-1) === '.' && key == '.') {
                return false;
            }
            else if (!input && state.result && ['%','('].indexOf(key) > -1) {
                setInput(state.result + key);
            }
            else {
                setInput(input + (op ? ' ' : '') + (key === ')' ? key + ' ' : key));
            }
            setOp('');
        }

        return key;
    }

    const onSubmit = async  () => {
        let expr = input.replace(/x/g, '*').replace(/÷/g, '/');
        dispatch(calculator.actions.request(expr));
        dispatch((
            ajax({
                url: "/calculator/calculate",
                method: 'POST',
                data: { data: expr } as any,
                onSuccess: calculator.actions.success
                ,
                onFailure: calculator.actions.error
                ,
                //label: setCalculatorRequest.type
            })
        ))
    };

    return (
        <>
            <IonContent>
                <IonGrid>
                    <IonRow style={{ height: '100%' }} className="ion-justify-content-center ion-align-items-center">
                        <IonCol sizeSm="10" sizeMd="6">
                            <IonRow>
                                <IonCol size="12">
                                    {/* <div>{`Resultat value ${res}`}</div> */}
                                    <div className="ion-text-right ion-padding ion-border last-input" style={{ "color": "black", "backgroundColor": "#eee", "fontSize": "0.5em" }}>
                                        {state.prev.replace(/\*/g, 'x').replace(/\//g, '÷') || ''}
                                    </div>
                                    <div className="ion-text-right ion-padding ion-border result" style={{ "color": "black", "backgroundColor": "#eee", "fontSize": "1.4em", "fontWeight": "500" }}>{(input || state.result || '0').replace(/\*/g, 'x').replace(/\//g, '÷')}</div>
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="secondary" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler('(')} >(</IonButton>
                                </IonCol>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="secondary" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler(')')} >)</IonButton>
                                </IonCol>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="warning" type="button" disabled={input === '' || input === state.result} onClick={() => onChangeHandler('CE')} >CE</IonButton>
                                </IonCol>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="danger" type="button" disabled={state.result === ''} onClick={() => onChangeHandler('AC')} >AC</IonButton>
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="light" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler('7')} >7</IonButton>
                                </IonCol>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="light" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler('8')} >8</IonButton>
                                </IonCol>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="light" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler('9')} >9</IonButton>
                                </IonCol>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="light" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler('÷')} >÷</IonButton>
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="light" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler('4')} >4</IonButton>
                                </IonCol>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="light" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler('5')} >5</IonButton>
                                </IonCol>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="light" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler('6')} >6</IonButton>
                                </IonCol>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="light" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler('x')} >x</IonButton>
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="light" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler('1')} >1</IonButton>
                                </IonCol>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="light" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler('2')} >2</IonButton>
                                </IonCol>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="light" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler('3')} >3</IonButton>
                                </IonCol>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="light" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler('+')} >+</IonButton>
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="light" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler('0')} >0</IonButton>
                                </IonCol>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="light" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler('.')} >.</IonButton>
                                </IonCol>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="light" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler('%')} >%</IonButton>
                                </IonCol>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="light" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler('-')} >-</IonButton>
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol size="9">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="primary" type="button" disabled={(input.match(/\(/g) || []).length !== (input.match(/\)/g) || []).length || op !== '' }  onClick={onSubmit} >=</IonButton>
                                </IonCol>
                                <IonCol size="3">
                                    <IonButton expand="block" size="large" strong={true} fill="solid" color="light" type="button" /* disabled={formState.isValid === false} */ onClick={() => onChangeHandler('^')} >^</IonButton>
                                </IonCol>
                            </IonRow>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </>
    );
};
export default Calculator;