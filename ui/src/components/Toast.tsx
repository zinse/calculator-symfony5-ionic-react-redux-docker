import PropTypes from "prop-types";
import React, { Component, useState  } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { IonToast } from "@ionic/react";
import { toast } from "../slices/toast";

type Position = "bottom" | "middle" | "top";
interface ToastProps {
  id: string;
  title?: string;
  message: string;
  color?: "primary" | "success" | "warning" | "danger" | "light" | "dark";
  position?: Position;
  autoClose?: boolean;
  duration?: number
};


export const Toast: React.FC<ToastProps> = (props: any) =>  {

  const {id, title, message, color, position, duration} = props

  const dispatch = useDispatch();
  const handleDismiss = () => {
    setShowToast(false)
    dispatch(toast.actions.remove(id));
  }
  const [showToast, setShowToast] = useState(true);

  return (

    <IonToast
      isOpen={showToast}
      color={color}
      onDidDismiss={() => handleDismiss()}
      header={title}
      message={message}
      position={position}
      duration={duration || 2000}
      buttons={[
        {
          side: 'end',
          icon: 'close',
          //text: 'Close',
          role: 'cancel',
          handler: () => {
            
          }
        }
      ]}
    />
    );
  
}

Toast.defaultProps = {
  color: "dark",
  position: "bottom" as Position,
  duration: 3000,
  autoClose: true
}